var updateHTML = function (jsonData) {
	document.getElementById('grid').innerHTML = "";
	
	for (tenant in jsonData) {
		thisTenant = document.createElement('div');
		thisTenant.setAttribute('class', 'bs-docs-grid');
		
		tenantAnchor = document.createElement('a');
		tenantAnchor.setAttribute('name', tenant);
		tenantAnchor.innerHTML = "Tenant " + tenant;
		
		tenantHeading = document.createElement('h2');
		tenantHeading.appendChild(tenantAnchor);
		thisTenant.appendChild(tenantHeading);
		
		row = document.createElement('div');
		row.setAttribute('class', 'row-fluid show-grid'); 
	
		numVM = 0;
		
		for (VM in jsonData[tenant]) {		
			numVM += 1;
			
			vm = document.createElement('div');
			vm.setAttribute('class', 'span3'); 
			
			general = document.createElement('div');
			general.innerHTML = '<h3>VM ' + VM + '</h3><b><u>Current Status: ' + jsonData[tenant][VM]['current_state'] + '</u></b>';
			general.innerHTML += '<h5><b>History</b></h5>';

			pChart = document.createElement('div');
			pChart.setAttribute('id', 'piechart_' + tenant + '_' + VM);
			
			vm.appendChild(general);
			vm.appendChild(pChart);
			
			row.appendChild(vm);
			console.log ("Appended " + VM + " to row");
			
			if (numVM % 4 == 0) {
				thisTenant.appendChild(row);
				row = document.createElement('div');
				row.setAttribute('class', 'row-fluid show-grid');
			}
		}
		thisTenant.appendChild(row);
		document.getElementById('grid').appendChild(thisTenant);
	}
	
	drawCharts(jsonData);
}

var drawThisChart = function (data, pChartID) {
	console.log(data);
	nv.addGraph(function() {
			var chart = nv.models.pieChart()
				.x(function(d) { return d.label })
				.y(function(d) { return d.value })
				.color(['green', 'red', 'yellow'])
				.showLabels(true);

			d3.select(pChartID)
				.append("svg:svg")
				.attr("width", 150)
				.attr("height", 150)
				.datum(data)
				.transition().duration(350)
				.call(chart);
			
			// nv.utils.windowResize(chart.update);

			return chart;
		});
}

var drawCharts = function (data) {
	for (tenant in data) {
		for (VM in data[tenant]) {
			drawThisChart ([{"label":"OK", "value": data[tenant][VM]["OK"]}, {"label":"Alarming!", "value": data[tenant][VM]["Alarming"]}, {"label":"Insufficient Data", "value": data[tenant][VM]["InsufficientData"]}], "#piechart_" + tenant + "_" + VM)
		}
	}
	
}

$.ajax({
	'url': 'http://10.0.0.199:5623/?request=data',
	'dataType': "json",
	'success': function (data) {
		if (jQuery.isEmptyObject(data))
			document.getElementById('grid').innerHTML = "<h3>Data Unavailable!</h3>";
		else
			updateHTML (data);
	},
	'error': function (xhr, ajaxOptions, thrownError){
		console.log (thrownError);
		document.getElementById('grid').innerHTML = "<h3>Data Unavailable!</h3>";
	},
   'timeout' : 15000 // timeout of the ajax call
});

