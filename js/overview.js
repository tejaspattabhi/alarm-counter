var updateHTML = function (jsonData) {
	var numTenant = 0;
	
	row = document.createElement('div');
	row.setAttribute('class', 'row-fluid show-grid'); 
	
	document.getElementById('grid').innerHTML = "";
	
	for (Tenant in jsonData) {		
		numTenant += 1;
		
		thisTenant = document.createElement('div');
		thisTenant.setAttribute('class', 'span5'); 
		
		tenantName = document.createElement('h2');
		tenantName.innerHTML = "Tenant " + Tenant;
		
		general = document.createElement('div');
		general.setAttribute('class', 'table-responsive'); 
		
		table = document.createElement('table');
		table.setAttribute('class', 'table table-bordered');
		table.innerHTML = "<thead><tr><th>VM</th><th>Current State</th><th>Alarm</th><th>OK</th><th>Insufficient Data</th></tr></thead>";
		table.innerHTML += "<tbody>";
		for (VM in jsonData[Tenant])
			table.innerHTML += "<tr><th>" + VM + "</th><td>" + jsonData[Tenant][VM]["current_state"] + "</td><td>" + jsonData[Tenant][VM]["Alarming"] + "</td><td>" + jsonData[Tenant][VM]["OK"] + "</td><td>" + jsonData[Tenant][VM]["InsufficientData"] + "</td></tr>";		
		table.innerHTML += "</tbody>";
		
		details = document.createElement('a');
		details.setAttribute('href', './detailed.html#' + Tenant); 
		details.innerHTML = "Details&nbsp;";
		
		general.appendChild(table);
		
		flush = document.createElement('a');
		flush.setAttribute('href', './flush.html?flush=' + Tenant); 
		flush.innerHTML = "&nbsp;Flush the contents of this tenant";
		
		general.appendChild(table);
		thisTenant.appendChild(tenantName);
		thisTenant.appendChild(general);
		thisTenant.appendChild(details);
		thisTenant.appendChild(flush);
		
		row.appendChild(thisTenant);
		
		if (numTenant % 2 == 0) {
			document.getElementById('grid').appendChild(row);
			row = document.createElement('div');
			row.setAttribute('class', 'row-fluid show-grid');
		}
	}
	document.getElementById('grid').appendChild(row);
	
	flushAll = document.createElement('a');
	flushAll.setAttribute('href', './flush.html?flush=all'); 
	flushAll.innerHTML = "Flush all tenants";
	document.getElementById('grid').appendChild(flushAll);
}

$.ajax({
	'url': 'http://10.0.0.199:5623/?request=data',
	'dataType': "json",
	'success': function (data) {
		if (jQuery.isEmptyObject(data))
			document.getElementById('grid').innerHTML = "<h3>Data Unavailable!</h3>";
		else
			updateHTML (data);
	},
	'error': function (xhr, ajaxOptions, thrownError){
		console.log (thrownError);
		document.getElementById('grid').innerHTML = "<h3>Data Unavailable!</h3>";
	},
   'timeout' : 15000 // timeout of the ajax call
});

