function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}    

var flush = getUrlParameter('flush');
$.ajax({
	'url': 'http://10.0.0.199:5623/?flush=' + flush,
	'dataType': 'html',
	'success': function (data) {
		if (jQuery.isEmptyObject(data))
			document.getElementById('grid').innerHTML = "<h3>Data Unavailable!</h3>";
		else
			document.getElementById('grid').innerHTML = data;
		
		document.getElementById('grid').innerHTML += '<a href="./index.html">Click Here to Go Back!</a>';
	},
	'error': function (xhr, ajaxOptions, thrownError){
		console.log (thrownError);
		document.getElementById('grid').innerHTML = "<h3>Action Failed!</h3>";
	},
   'timeout' : 15000 // timeout of the ajax call
});
