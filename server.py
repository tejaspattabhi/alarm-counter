##################################################################################
# Name		:	Server Component of Alarm Counter Application
# Description	:	Listen's to port 5623, for two kinds of web requests
# 			1. POST: Data coming from a webhook - ceilometer alarm
# 			2. GET: Request coming from a client/browser requesting
#                               data posted by the webhooks
# Function	: 	Collects data from different webhooks, aggregates it
#			and maintains it in the memory
# Author	: 	Tejas Tovinkere Pattabhi
# Last Updated	: 	07/22/2015
##################################################################################

from BaseHTTPServer import BaseHTTPRequestHandler
import urlparse
import json

# Data is the dictionary where the aggregated data from webhooks are stored. It acts like a hashmap
data = {}

# GetHandler is the class overriding the methods for GET and POST requests to perform the described actions
class GetHandler(BaseHTTPRequestHandler):
	# do_GET is the method which is executed when there is a request from client. 
	# Function: Posting Data as JSON, clearing a Tenant's/all data
	def do_GET(self):
		global data

		parsed_path = urlparse.urlparse(self.path)
		# params is a dictionary which has the paramters split as : params['request'] or params['flush']
		# Example: if http://10.0.0.199/?request=data ; params['request'] = 'data'
		# Example: if http://10.0.0.199/?flush=all/X  ; params['flush'] = 'all' or 'X'
		
		message = '<html><head><title>Unknown Request</title></head><body><h1>I\'ve no idea what you are talking about!</h1></body></html>'
		try:
			params = dict([p.split('=') for p in (parsed_path.query).split('&')])
			print params
		except:
			params = {}
		
		if params != {}:
			# If the request is to flush, then it flushes all tenants or just the tenant name as specified.
			if 'flush' in params: 
                                message = '<html><head><title>Clearing Data</title></head><body><h1>Clearing JSON</h1></body></html>'
                                if params['flush'] == 'all':
                                        data = {}
                                else:
                                        try: 
                                                del data[params['flush']]
                                        except:
                                                print ""
			# If the request is to return JSON, it dumps 'data' to JSON and returns it
			if 'request' in params:
				message = json.dumps (data)
	
		# These are the headers which are required to return the request as JSON
		self.send_response(200)
                self.send_header("Access-Control-Allow-Origin", "*")
                self.send_header('Content-Type', 'application/json')
                # self.send_header('Content-Type', 'text/event-stream')	
		self.end_headers()
		self.wfile.write(message)
		return
		
	def do_POST(self):
		global data

		# params is a dictionary which has the paramters split as : params['tenant'], params['alarm'], params['VMID']
		# Example: if http://10.0.0.199/?tenant=X&alarm=OK&VMID=1 ; params['tenant'] = 'X', params['alarm'] = 'OK', params['VMID'] = 1

		parsed_path = urlparse.urlparse(self.path)
		message = '<html><head><title>Success</title></head><body><h1>Data Accepted</h1></body></html>'
		try:
			params = dict([p.split('=') for p in (parsed_path.query).split('&')])
			print params
		except:
			params = {}
	
		# The structure of data as a Data Structure is explained in the document. Kindly refer that if clarification required	
		if params != {}:
			print json.dumps (data)
			if 'request' in params: # This case shouldn't occur as the VM GETs a request
				message = json.dumps (data)
				self.send_response(200)
				self.send_header("Access-Control-Allow-Origin", "*")
				self.send_header('Content-Type', 'application/json')
				# self.send_header('Content-Type', 'text/event-stream')
			elif params['tenant'] in data:
				if params['VMID'] in data[params['tenant']]:
					data[params['tenant']][params['VMID']]['current_state'] = params['alarm']
					data[params['tenant']][params['VMID']][params['alarm']] += 1
				else:
					data[params['tenant']][params['VMID']] = {}
					data[params['tenant']][params['VMID']]['current_state'] = params['alarm']
					data[params['tenant']][params['VMID']]['OK'] = 0
					data[params['tenant']][params['VMID']]['InsufficientData'] = 0
					data[params['tenant']][params['VMID']]['Alarming'] = 0
					data[params['tenant']][params['VMID']][params['alarm']] += 1
				self.send_response(200)
			else:
				data[params['tenant']] = {}
				data[params['tenant']][params['VMID']] = {}
				data[params['tenant']][params['VMID']]['current_state'] = params['alarm']
				data[params['tenant']][params['VMID']]['OK'] = 0
				data[params['tenant']][params['VMID']]['InsufficientData'] = 0
				data[params['tenant']][params['VMID']]['Alarming'] = 0
				data[params['tenant']][params['VMID']][params['alarm']] += 1
				self.send_response(200)
		
		self.end_headers()
		self.wfile.write(message)
		return

if __name__ == '__main__':
    # Setup a basic HTTP server
    from BaseHTTPServer import HTTPServer
    # Not mentioning the hostname/IP address makes it bind to the local machine.
    server = HTTPServer(('', 5623), GetHandler)
    print 'Starting server, use <Ctrl-C> to stop'
    server.serve_forever()
